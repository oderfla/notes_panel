import React from 'react';
import ReactDOM from 'react-dom';
import Notes from './container/notesPanel';

ReactDOM.render(<Notes />, document.querySelector('#root'));