import React, {useEffect} from "react";
import PropTypes from 'prop-types';
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import styles from "./BottomToolBarStyles";
import Button from '@material-ui/core/Button';
// import Fab from "@material-ui/core/Fab";
// import AddIcon from "@material-ui/icons/Add";
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';

// const CustomCheckbox = withStyles({
//   root: {
//     color: 'rgba(0, 0, 0, 0.54)',
//     '&$checked': {
//       color: '#008476',
//     },
//   },
//   checked: {},
// })(props => <Checkbox color="default" {...props} />);

export const BottomToolBar = props => {
  const { classes } = props;
  const [printDisable, setPrintDisable] = React.useState(false);

  useEffect(() => {
    props.printCardCount > 0 ? setPrintDisable(false) : setPrintDisable(true);
  }, [props.printCardCount]);

  return (
    <React.Fragment>
    <div className={classes.grow}>
      <AppBar color="primary" className={classes.appBar}>
        {props.printSelected ? (
          <Toolbar>
            <Button aria-label="cancel" className={classes.cancelButton} onClick={props.cancelPrint}>
              Cancel
            </Button>
            <div>
              <Button aria-label="add" size="medium" disabled={printDisable} className={classes.printSelectedButton} classes={{ disabled: classes.printSelectedDisabledButton }}>
                Print Selected
              </Button>
            </div>
          </Toolbar>) : 
          // (<Toolbar>
          //   <FormControlLabel className={classes.label}
          //     control={<CustomCheckbox value="all" />}
          //     label="View all notes"
          //   />
          //   <Fab aria-label="add" className={classes.fabButton} disableRipple>
          //     <AddIcon />
          //   </Fab>
          // </Toolbar>)}
          (null)}
      </AppBar>
      </div>
    </React.Fragment>
  );
}

BottomToolBar.propTypes = {
  classes: PropTypes.object,
};

//Exporting the component this way allows us to show the PropTypes without the withStyles wrapper
const bottomControlToolBar = withStyles(styles)(BottomToolBar);
bottomControlToolBar.displayName = 'BottomToolBar';
bottomControlToolBar.propTypes = BottomToolBar.propTypes;

export default bottomControlToolBar;