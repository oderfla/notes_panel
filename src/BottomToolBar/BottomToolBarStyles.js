const styles = theme => ({

    appBar: {
      top: "auto",
      bottom: 0,
      left:0,
      width: 'inherit',
      height:'48px',
      boxShadow: '0px -2px 2px grey',
      backgroundColor: '#F5F5F5',
      position: 'aboslute'
    },
    grow: {
      position: 'fixed',
      bottom: 0,
      height: '48px',
      width: '410px',
      backgroundColor: '#F5F5F5',
    },
    fabButton: {
      position: "absolute",
      zIndex: 1,
      top: -22,
      right: 22,
      height: '40px',
      width: '40px',
      margin: "0 auto",
      backgroundColor: "#008476",
      color: "white"
    },
    toggleButton: {
      top: -8,
      color: 'black'
    },
    label: {
      color: 'rgba(0, 0, 0, 0.54)',
      margin : '0px 0px 15px -5px',
    },
    printSelectedButton: {
      textTransform: 'none',
      position: "absolute",
      bottom: '22px',
      left: '110px',
      width: '140px',
      backgroundColor: "#008476",
      color: "white",
    },
    printSelectedDisabledButton: {
      textTransform: 'none',
      position: "absolute",
      bottom: '22px',
      left: '110px',
      width: '140px',
      backgroundColor: "#CBCDCF",
      '&:disabled': {
        color: 'white'
    }
    },
    cancelButton: {
      margin: theme.spacing(1),
      textTransform: 'none',
      position: "absolute",
      bottom: '14px',
      left: '5px',
      // height: '28px',
      width: '96px',
      color: "#008476"
    },
  });

  export default styles;