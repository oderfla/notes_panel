import React from "react";
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import PrintIcon from "@material-ui/icons/Print";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Badge from '@material-ui/core/Badge';
import styles from "./TopToolBarStyles";
import {
  PRINT,
  PRINT_ALL,
  WHITE,
  GREEN,
  BLACK,
  SHADOW,
  TEACHER_NOTES,
  USER_NOTES,
  USER_BOOKMARKS
} from '../utils/constants.js';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CardsContainer from '../CardsContainer/cardsContainer.js';
import BottomToolBar from '../BottomToolBar/bottomToolBar.js';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    height: '100%',
    position: 'overflow',
  },
  tabVisible: {
    boxShadow: '0px 3px 2px 0px ' + SHADOW,
  },
  tabHidden: {
    visibility: 'hidden',
    boxShadow: '0px 3px 2px 0px ' + SHADOW,
  },
});

//Overall tab parent element style
const AntTabs = withStyles({
  root: {
    flexGrow: 1,
    backgroundColor: WHITE,
    width: '100%',
    height: '48px',
  },
  indicator: {
    backgroundColor: GREEN,
    height:'3px'
  },
})(Tabs);

// theme that is getting used for each one of the tabs individually
const AntTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    minWidth: 126,
    fontWeight: 500,
    color: BLACK,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&$selected': {
      color: GREEN,
      fontWeight: 500,
    },
    '&:focus': {
      color: GREEN,
      fontWeight: 500,
    },
  },
  selected: {},
}))(props => <Tab disableRipple {...props} />);

export const TopToolBar = props => {
  const tabClasses = useStyles();
  const [value, setValue] = React.useState(0);
  const { classes } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [searchBarVisible, setsearchBarVisible] = React.useState(false);
  const [searchTerm, setSearchTerm] = React.useState("");
  const [filteredCards, setFilteredCards] = React.useState([]);
  const [isPrintSelected, setPrintSelected] = React.useState(false);
  const [printCount, setPrintCount] = React.useState(0);
  const [printCards, setPrintCards] = React.useState([]);


  const handlePrintCount = (card) => {    
    card.print ? printCards.push(card) : printCards.pop();
    setPrintCards(printCards);
    setPrintCount(printCards.length);
  }

  const cancelPrint = () => {    
    printCards.forEach(card => card.print = false);
    setPrintSelected(false);
    setPrintCards([]);
    setPrintCount(0);
  }
  
  const searchBoxRef = React.useRef(null);

  const isSettingsOpen = Boolean(anchorEl);

  function handleSettingsMenuOpen(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleMenuClose(event) {
    console.log("Option Selected:" + event.currentTarget.id);
    if (event.currentTarget.id === 'Select to Print'){
      setPrintSelected(true);
    }
    setAnchorEl(null);
  }

  function handleCloseAction(event) {
    if (searchBarVisible) {
      //we are finishing the search instance
      setsearchBarVisible(!searchBarVisible);
      searchBoxRef.current.value = "";
      setSearchTerm("");
    }
    else{
      //we need to send a message to close the panel
    }
  }

  function handleSearchBarShowing(target) {
    //we use the button to only hide and show the search bar
    setsearchBarVisible(!searchBarVisible);
    setTimeout(() => {
      searchBoxRef.current.focus();
    }, 100);
  }

  function textEntered(event) {
    setSearchTerm(event.currentTarget.value);
    if(event.currentTarget.value.length >= 3){
      filterCards(event.currentTarget.value);
    }
  }

  function handleTabChange(event, newValue) {
    setValue(newValue);
    printCards.forEach(card => card.print = false);
    setPrintCards([]);
    setPrintCount(0);
  }

  function filterCards(searchText) {
    let allData = props.allData;
    let filteredData = [];
    let ete = [];
    let notes = [];
    let bookmarks = [];
    allData = allData.filter((card) => {
      return card.message.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      })
      allData.forEach(element => {
      switch (element.type) {
        case TEACHER_NOTES:
          ete.push(element);
          break;
        case USER_NOTES:
          notes.push(element);
          break;
        case USER_BOOKMARKS:
          bookmarks.push(element);
          break;
        default:
          break;
      }
    });

    filteredData[TEACHER_NOTES] = ete;
    filteredData[USER_NOTES] = notes;
    filteredData[USER_BOOKMARKS] = bookmarks;

    setFilteredCards(filteredData);
  }

  const settingsId = "primary-settings-menu";
  const renderSettings = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={settingsId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isSettingsOpen}
      onClose={handleMenuClose}
    >
      <MenuItem id={PRINT_ALL} onClick={handleMenuClose}>{PRINT_ALL}</MenuItem>
      <MenuItem id={PRINT} onClick={handleMenuClose}>{PRINT}</MenuItem>
    </Menu>
  );

  return (
    <div position="fixed" className={classes.container}>
      <div position="fixed" className={classes.topContainer}>
        <Toolbar 
        aria-label="Top Tool Bar"
        className={classes.toolBar}>
          
          <div className={!searchBarVisible ? classes.searchPlaceholderHidden : classes.searchPlaceholder}>
            <SearchIcon aria-label="search"/>
          </div>
          <div id='searchBar' hidden={!searchBarVisible} className={classes.search}>
            <InputBase
              placeholder="Search notes & bookmarks"
              inputRef={searchBoxRef}
              onChange={textEntered}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
              // inputProps={{ "aria-label": "Search" }}
            />
          </div>
          <div className={classes.optionsContainer}>
            <IconButton className={searchBarVisible ? classes.iconButtonHidden : classes.iconButton}
              aria-label="Search" 
              onClick={handleSearchBarShowing}>
              <SearchIcon/>
            </IconButton>
            <IconButton className={/*searchBarVisible ? classes.iconButtonHidden : */classes.iconButton}
              aria-label="more options" 
              aria-controls={settingsId}
              aria-haspopup="true"
              onClick={handleSettingsMenuOpen}>
                <Badge color="secondary" badgeContent={printCount}>
                  <PrintIcon/>
                </Badge>
            </IconButton>
            <IconButton className={classes.iconButton}
              aria-label="close panel"
              onClick={handleCloseAction}>
              <CloseIcon/>
            </IconButton>
          </div>
        </Toolbar>
        <div className={tabClasses.root}>
          <div className={searchBarVisible ? tabClasses.tabHidden : tabClasses.tabVisible}>
            <AntTabs value={value} onChange={handleTabChange} aria-label="ant example" centered>
              {props.tabNames.map((tab, index) => {
                return <AntTab key={index} label={tab} />;
              })}
            </AntTabs>
          </div>
        </div>
      </div>
          <CardsContainer
            selectedTab={props.tabNames[value]}
            data={props.parsedData}
            searchTerm={searchTerm}
            filteredCards={filteredCards}
            isTeacher={props.isTeacher}
            printSelected={isPrintSelected}
            handlePrintCount={handlePrintCount}
          />
          <BottomToolBar
          printSelected={isPrintSelected}
          printCardCount={printCount}
          cancelPrint={cancelPrint}/>
        {renderSettings}
      </div>
  );
};

TopToolBar.propTypes = {
  classes: PropTypes.object,
};

//Exporting the component this way allows us to show the PropTypes without the withStyles wrapper
const topControlToolBar = withStyles(styles)(TopToolBar);
topControlToolBar.displayName = 'TopToolBar';
topControlToolBar.propTypes = TopToolBar.propTypes;

export default topControlToolBar;