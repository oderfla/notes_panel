import {WHITE, ICONS_COLOR, SHADOW} from '../utils/constants.js';

const styles = theme => ({
  toolBar: {
    minHeight: 48,
    backgroundColor: WHITE,
  },
  container: {
    backgroundColor: WHITE,
    top: '0',
    height: '100%',
  },
  topContainer: {
    backgroundColor: WHITE,
    top: '0',
    height: '96px',
    boxShadow: '0px 15px 10px -15px ' + SHADOW
  },
  search: {
    position: 'absolute',
    top: 6,
    left: 10,
    borderBottom: '1px solid #8f9496',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: WHITE,
    "&:hover": {
      backgroundColor: WHITE
    },
    marginRight: theme.spacing(1),
    width: 290,
    zIndex: 2,
    [theme.breakpoints.up("sm")]: {
      // marginLeft: 0,
    //   width: "auto"
    }
  },
  inputRoot: {
    backgroundColor: WHITE,
    width: 280,
    left:20,
    zIndex: 1,
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 1),
    // transition: theme.transitions.create("width"),
    width: '280px',
    [theme.breakpoints.up("md")]: {
      width: '280px'
    }
  },
  optionsContainer: {
    position: "absolute",
    height: '48px',
    width: '145px',
    top: 0,
    right: 0,
    backgroundColor: WHITE
  },
  iconButton: {
    color: ICONS_COLOR,
    // '&:hover': {
    //   color: red[800],
    // },
  },
  iconButtonHidden: {
    visibility: 'hidden',
    color: ICONS_COLOR,
  },
  searchPlaceholder: {
    color: ICONS_COLOR,
    width:24,
    height:24,
    marginLeft:-12,
    zIndex:3,
    // '&:hover': {
    //   color: red[800],
    // },
  },
  searchPlaceholderHidden: {
    visibility: 'hidden',
    color: ICONS_COLOR,
    width:24,
    height:24,
    marginLeft:-12,
    zIndex:3,
    // '&:hover': {
    //   color: red[800],
    // },
  },
});

export default styles;