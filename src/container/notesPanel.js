import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TopToolBar from '../TopToolBar/topToolBar.js';
import CssBaseline from '@material-ui/core/CssBaseline';
import cutText from '../utils/cut-text.js';
import {
  CLASSROOM,
  MY_NOTES,
  BOOKMARK,
  WHITE,
  TEACHER_NOTES,
  SHADOW,
} from '../utils/constants.js';

const NotesContainer = withStyles({
  root: {
    flexGrow: 1,
    backgroundColor: WHITE,
    width: '410px',
    height: '100%',
    margin: '0',
    repositionOnUpdate: 'false',
    padding: '0px 0px 0px 0px',
    position: 'fixed',
    boxShadow: '2px 2px 10px 2px ' + SHADOW ,
  },
})(Container);

const message1 = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a malesuada tellus.
 Integer sapien neque, laoreet non ultrices sit amet, dictum sit amet velit. Curabitur scelerisque
  ligula non tellus rutrum vehicula. Mauris nunc est, rutrum ut urna id, mattis vehicula erat. 
  Vivamus malesuada est purus, a hendrerit massa semper sed. Nam a turpis a sapien rhoncus auctor sed id enim. 
  Aliquam tristique purus et urna placerat, vel condimentum libero porttitor.`;

const message2 = `Integer dapibus ipsum sagittis lacus bibendum ullamcorper. Vivamus velit elit, iaculis ac efficitur non, 
  pharetra ac metus. Etiam ut posuere elit. Proin in blandit lectus, non sollicitudin erat. Nullam vel dapibus lacus. 
  Nam sagittis, mauris sit amet elementum eleifend, tellus nulla tempor velit, quis gravida metus augue a nisl. 
  áIn dignissim justo eget ligula pharetra mollis a nec elit.`;

const message3 = `Integer dapibus ipsum sagittis lacus bibendum ullamcorper. Vivamus velit elit, iaculis ac efficitur non, 
  pharetra ac metus. Etiam ut posuere elit. Proin in blandit lectus, non sollicitudin erat.`;

const mockData = [];
for (var i=0; i<100; i++){
  mockData.push({
    message: message1,
    active: false,
    type: 99,
    print: false,
  });
}
const myNotesData = [
  {
    message: "one",
    active: false,
    note: "Note 1",
    noteEnabled: true,
    type: 1,
    print: false,
  },
  {
    message: "one two",
    active: false,
    note: "Note 2",
    noteEnabled: true,
    type: 1,
    print: false,
  },
  {
    message: "one two three",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four, five",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four, five, six",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four, five, six, seven",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four, five, six, seven, eigth",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four, five, six, seven, eight, nine",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: "one two, three four, five, six, seven, eight, nine, ten",
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message1,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message2,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message3,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message1,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message3,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message2,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  },
  {
    message: message1,
    active: false,
    note: "",
    noteEnabled: false,
    type: 1,
    print: false,
  }
];

const bookmarksData = [
  {
    message: "Bookmark A number one",
    active: false,
    note: "",
    noteEnabled: false,
    type: 2,
    print: false,
  },
  {
    message: "Bookmark B number two",
    active: false,
    note: "Boomark note 2",
    noteEnabled: true,
    type: 2,
    print: false,
  },
  {
    message: "Bookmark C number three",
    active: false,
    note: "",
    noteEnabled: false,
    type: 2,
    print: false,
  },
  {
    message: "Bookmark D number four",
    active: false,
    note: "",
    noteEnabled: false,
    type: 2,
    print: false,
  },
  {
    message: "Bookmark E number five",
    active: false,
    note: "",
    noteEnabled: false,
    type: 2,
    print: false,
  },
  {
    message: "Bookmark F number six",
    active: false,
    note: "",
    noteEnabled: false,
    type: 2,
    print: false,
  }
];

const data = require('../annotations.json');
const parsed = getTeacherAnnotationsGutter(data);
console.log(parsed);

const isTeacher = true;
var keys = [CLASSROOM, MY_NOTES, BOOKMARK];

const dict = [];
dict[keys[0]] = parsed.allData;
dict[keys[1]] = myNotesData;
dict[keys[2]] = bookmarksData;

if (!isTeacher) {
  keys.shift();
  dict[keys[0]] = myNotesData
  dict[keys[1]] = bookmarksData;
}
else{
  dict[keys[0]] = parsed.allData;
  dict[keys[1]] = myNotesData;
  dict[keys[2]] = bookmarksData;
}

export default function NotesPanel() {
  return (
    <React.Fragment>
      <CssBaseline />
      <NotesContainer>
        <TopToolBar      
          tabNames={keys}
          parsedData={dict}
          allData={parsed.allData.concat(myNotesData).concat(bookmarksData)}
          isTeacher={isTeacher}
        />
      </NotesContainer>
    </React.Fragment>
  );
}

//         <BottomToolBar/>
function getTeacherAnnotationsGutter(response) {
  const INDEX_MAX_NOTES = 10000;
  const list = response.annotations.map((record, index) => {
    let newIndex = index + INDEX_MAX_NOTES;
    return {
      id: newIndex,
      parent_id: record.hasTarget.hasSelector[0].value.split('-')[2],
      spine_id: record.hasTarget.hasSource['@id'],
    };
  })

  const byAllData = response.annotations.map((record, index) => {
    let newIndex = index + INDEX_MAX_NOTES;
    const { CFI: cfi } = record.hasTarget.hasSelector.find(item =>
      item.hasOwnProperty('CFI')
    );
    const noteColor = record.hasBody.color || 'inherit';

    return {
      annotation_id: `${Date.now()}-${Math.random()}`,
      message: record.hasBody.chars.replace("xml:lang=\"en\"", ""),
      body_text_cut: cutText(record.hasBody.chars, 150),
      cfi,
      content_id: record.hasTarget.hasSource['@id'],
      id: newIndex,
      isLoading: false,
      isReady: true,
      note: {
        isLoading: false,
        isReady: true,
        body_text: '',
        editing: false,
      },
      object_id: record.hasTarget.hasSelector[0].value.split('-')[2],
      path: record.hasTarget.hasSource['@id'],
      showFullNote: false,
      teacherNoteStyle: `
       .tn-${newIndex} span, .tn-${newIndex} b, .tn-${newIndex} i, .tn-${newIndex} strong  { display: inline !important; }
       .tn-${newIndex} li { display: list-item; list-style: disc outside none; margin-left: 10px; padding-bottom: 10px; }
       .tn-${newIndex} { font-family: Lato; }
       .tn-${newIndex} h3, .tn-${newIndex} h1, .tn-${newIndex} h2  { color: ${noteColor}; font-size: 21px; margin-bottom: 20px; line-height: 1.2;}
       .tn-${newIndex} p { margin-bottom: 20px; font-size: 15px; line-height: 1.4; display: inline; }
       `,
      className: `tn-${newIndex}`,
      title: record.hasBody.chars,
      type: TEACHER_NOTES,
      print: false
    };
  })

  return {
    allData : byAllData,
    list : list,
    bySpineId : list.reduce((acc, v) => {
      acc[v.spine_id] = acc[v.spine_id] || [];
      acc[v.spine_id].push(v);
      return acc;
    }, {})
  }
}
