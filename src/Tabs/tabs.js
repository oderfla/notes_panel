import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CardsContainer from '../CardsContainer/cardsContainer.js';
import {
  WHITE,
  GREEN,
  BLACK,
  SHADOW,
} from '../utils/constants.js';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    height: '100%',
    position: 'overflow',
  },
  tab: {
    boxShadow: '0px 3px 2px 0px ' + SHADOW,
  },
});

//Overall tab parent element style
const AntTabs = withStyles({
  root: {
    flexGrow: 1,
    backgroundColor: WHITE,
    width: '100%',
    height: '48px',
  },
  indicator: {
    backgroundColor: GREEN,
    height:'3px'
  },
})(Tabs);

// theme that is getting used for each one of the tabs individually
const AntTab = withStyles(theme => ({
  root: {
    textTransform: 'none',
    minWidth: 126,
    fontWeight: 500,
    color: BLACK,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&$selected': {
      color: GREEN,
      fontWeight: 500,
    },
    '&:focus': {
      color: GREEN,
      fontWeight: 500,
    },
  },
  selected: {},
}))(props => <Tab disableRipple {...props} />);

export default function NotesTab(props) {
  
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  return (
    <div className={classes.root}>
      <div className={classes.tab}>
        <AntTabs value={value} onChange={handleChange} aria-label="ant example" centered>
        {props.tabNames.map((tab, index) => {
            return <AntTab key={index} label={tab} />;
          })}
        </AntTabs>
      </div>
      <CardsContainer
        selectedTab={props.tabNames[value]}
        data={props.parsedData}
      />
    </div>
  );
}