import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import MoreIcon from "@material-ui/icons/MoreVertSharp";
import IconButton from "@material-ui/core/IconButton";
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';

const CssTextField = withStyles({
  root: {
    width: '340px',
    "& label.Mui-focused": {
      color: "#146d5f"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#146d5f"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#146d5f"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#146d5f"
      }
    }
  }
})(TextField);

const CustomCheckbox = withStyles({
  root: {
    color: 'rgba(0, 0, 0, 0.54)',
  },
})(props => <Checkbox {...props} />);

const CustomSelectedCheckbox = withStyles({
  root: {
    color: '#008476',
  },
})(props => <CircleCheckedFilled {...props} />);

export const BookmarkCard = props => {
    const { classes, card, index } = props;
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [activeCard, setActiveCard] = React.useState(true); //this is needed so the view updates after making a change
    const [noteValue, setNoteValue] = React.useState(card.note);
    const isSettingsOpen = Boolean(anchorEl);
    const textfieldRef = React.useRef(null);
  
    function handleSettingsMenuOpen(event) {
      setActiveCard(event.currentTarget.id);
      setAnchorEl(event.currentTarget);
    }
  
    function handleMenuClose(event) {
      setAnchorEl(null);
      if (event.currentTarget.id === "Add Note"){
        card.noteEnabled = true;
        setTimeout(() => {
          textfieldRef.current.focus();
        }, 100);
      }
      else if (event.currentTarget.id === "Remove"){
        props.setDeletedCard({type:card.type, card: card});
        //we need to remove the highlight/bookmark from the list
      }
    }
  
    function viewOnPage(event) {
      // we need to send a message to the content to navigate to that page
      console.log("VIEW ON PAGE");
    }
  
    const handleChange = event => {
      // detects onChange effect from textfield updating the field
      setNoteValue(event.currentTarget.value);
    };
  
    const handleBlur = event => {
      // this gets triggered when the textfield loses focus
      // we trim spaces and line breaks to validate we don't get empty notes
      if (event.target.value.replace(/^\s+|\s+$/g, '').length === 0){
        setNoteValue("");
        card.note = noteValue;
        card.noteEnabled = false;
        setActiveCard(null);
      }
      else{
        //card note was updated, we need to send this to CAS to be saved
        card.note = event.target.value;
      }
    }
  
    const handleFocus = event => {
      console.log(activeCard);
      setActiveCard(event.target.id);
    }
  
    const cardSelected = event => {
      card.print = !card.print;
      props.handlePrintCount(card);
    }
    
  const moreMenuOptions = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id="notes-more-menu"
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isSettingsOpen}
        onClose={handleMenuClose}
      >
      {!card.noteEnabled ? (
          <MenuItem id="Add Note" onClick={handleMenuClose}>Add Note</MenuItem>
      ) : null}
        <MenuItem id="Remove" onClick={handleMenuClose}>Remove Bookmark</MenuItem>
      </Menu>
    );

  return (
    <Paper key={index} className={(card.active ? classes.paperActive : classes.paper)}>
      <Grid container wrap="nowrap" className={classes.grid}>
        <Grid item xs>
          <Typography>{card.message}</Typography>
        </Grid>
      </Grid>
      <Grid hidden={!card.noteEnabled}>
      <CssTextField
        id={String(index)}
        style={{ margin: 2 }}
        multiline
        placeholder="Start typing..."
        value={noteValue}
        variant="outlined"
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        inputRef={textfieldRef}
        InputProps={{
            classes: {
              input: classes.textFont,
            },
          }}
        InputLabelProps={{
          shrink: true,
        }}
        />
      </Grid>
      <Button className={classes.viewOnPageButton} onClick={viewOnPage}>
        FIND ON PAGE
      </Button>
      {props.isPrintSelected ? (      
      <CustomCheckbox className={classes.printIcon}
        icon={card.print ? <CustomSelectedCheckbox /> : <CircleUnchecked />}
        checkedIcon={card.print ? <CircleUnchecked /> : <CustomSelectedCheckbox />}
        onChange={cardSelected}
      />) : (      
      <IconButton id={String(index)} className={classes.moreIcon} aria-label="more options" color="inherit" onClick={handleSettingsMenuOpen}>
      <MoreIcon />
    </IconButton>)}
      {moreMenuOptions}
    </Paper>
  );
}

export default BookmarkCard;