import React, {useEffect, useRef} from "react";
import PropTypes from 'prop-types';
import RootRef from '@material-ui/core/RootRef';
import { withStyles } from "@material-ui/core/styles";
import styles from './CardsContinerStyles';
import { Virtuoso } from 'react-virtuoso';
import {records} from './CardsDataHandler.js';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Card from './Card.js';
import {
  CLASSROOM,
  MY_NOTES,
  BOOKMARK,
  TEACHER_NOTES,
  USER_NOTES,
  USER_BOOKMARKS
} from '../utils/constants.js';

const VirtuosoStyle = {
  width: '410px',
  height: "100%",
};

export const CardsContainer = props => {

  const { classes, data, selectedTab, searchTerm, printSelected, handlePrintCount } = props;
  const [deletedCard, setDeletedCard] = React.useState({type:0, card: null});
  const {loadedUsers, loadMore, resetValues} = records(data[selectedTab], data[selectedTab].length, selectedTab, classes, setDeletedCard, printSelected, handlePrintCount);
  const domRef = React.useRef();
  const virtuoso = useRef(null);
  const [isSearchActive, setIsSearchActive] = React.useState(false);
  const [openEte, setOpenEte] = React.useState(false);
  const [openNotes, setOpenNotes] = React.useState(false);
  const [openBookmarks, setOpenBookmarks] = React.useState(false);

  const eteClick = () => {
    setOpenEte(!openEte);
  };
  const notesClick = () => {
    setOpenNotes(!openNotes);
  };
  const bookmarkClick = () => {
    setOpenBookmarks(!openBookmarks);
  };

  useEffect(() => {
    virtuoso.current.scrollToIndex({
      index: 0,
      align: 'Start',
    })
    resetValues();
    loadMore();
  }, [selectedTab]);

  useEffect(() => {
    resetValues();
    loadMore();
  }, [printSelected]);

  useEffect(() => {
    console.log("card needs to be deleted" + JSON.stringify(deletedCard));
  }, [deletedCard]);

  useEffect(() => {
    if (searchTerm.length >= 3) {
      setIsSearchActive(true);
      console.log("START FILTERING CARDS WITH: " + props.searchTerm);
    }
    else{
      setIsSearchActive(false);
      console.log("NOT ENOUGH DATA TO FILTER");
      setOpenEte(false); setOpenNotes(false); setOpenBookmarks(false);
      //we show all of the cards
    }
  }, [searchTerm]);

  return (
    <RootRef rootRef={domRef}>
      <div className={classes.root}>
      {!isSearchActive ? (
        <Virtuoso
          style={{ ...VirtuosoStyle}}
          totalCount={loadedUsers.length}
          ref={virtuoso}
          endReached={loadMore}
          overscan={1}
          item={(index, item) => <div>{loadedUsers[index]}</div>}
        />
      ) : 
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
      >
        {props.isTeacher ? (
        <div>
        <ListItem button onClick={eteClick}>
          <ListItemText primary={"Classroom (" + props.filteredCards[TEACHER_NOTES].length + ")"}/>
          {openEte ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={openEte} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {props.filteredCards[TEACHER_NOTES].map((card, index) => {
              if (card.type === TEACHER_NOTES){
                return (
                  <Card key={index}
                    card={card}
                    index={index}
                    classes={classes}
                    selectedTab={CLASSROOM}
                  />
                )
              }
              return null;
            }
            )}
          </List>
        </Collapse> 
        </div>
        ) : null}
        <ListItem button onClick={notesClick}>
          <ListItemText primary={"Notes (" + props.filteredCards[USER_NOTES].length + ")"} />
          {openNotes ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={openNotes} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {props.filteredCards[USER_NOTES].map((card, index) => {
              if (card.type === USER_NOTES){
                return (
                  <Card key={index}
                    card={card}
                    index={index}
                    classes={classes}
                    selectedTab={MY_NOTES}
                  />
                )
              }
              return null;
            }
            )}
          </List>
        </Collapse>
        <ListItem button onClick={bookmarkClick}>
          <ListItemText primary={"Bookmarks (" + props.filteredCards[USER_BOOKMARKS].length + ")" }/>
          {openBookmarks ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={openBookmarks} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {props.filteredCards[USER_BOOKMARKS].map((card, index) => {
              if (card.type === USER_BOOKMARKS){
                return (
                  <Card key={index}
                    card={card}
                    index={index}
                    classes={classes}
                    selectedTab={BOOKMARK}
                  />
                )
              }
              return null;
            }
            )}
          </List>
        </Collapse>
      </List> 
    }
    </div>
    </RootRef>
  );
}

CardsContainer.propTypes = {
  classes: PropTypes.object,
};

//Exporting the component this way allows us to show the PropTypes without the withStyles wrapper
const cardControlList = withStyles(styles)(CardsContainer);
cardControlList.displayName = 'CardsContainer';
cardControlList.propTypes = CardsContainer.propTypes;

export default cardControlList;