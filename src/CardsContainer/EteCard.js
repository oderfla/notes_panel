import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import ReactHtmlParser from 'react-html-parser';
import Checkbox from '@material-ui/core/Checkbox';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';

const CustomCheckbox = withStyles({
  root: {
    color: 'rgba(0, 0, 0, 0.54)',
  },
})(props => <Checkbox color="default" {...props} />);

const CustomSelectedCheckbox = withStyles({
  root: {
    color: '#008476',
  },
})(props => <CircleCheckedFilled {...props} />);

export const EteCard = props => {
  const { classes, card, index } = props;

  function viewOnPage(event) {
    // we need to send a message to the content to navigate to that page
    console.log("VIEW ON PAGE");
  }

  const cardSelected = event => {
    card.print = !card.print;
    props.handlePrintCount(card);
  }

  return (
    <Paper key={index} className={(card.active ? classes.paperActive : classes.paper)}> 
      <Grid container wrap="nowrap" className={classes.grid}>
        <Grid item xs>
          {ReactHtmlParser(card.message)}
        </Grid>
      </Grid>
      <Button className={classes.viewOnPageButton} onClick={viewOnPage}>
        FIND ON PAGE
      </Button>
      {props.isPrintSelected ? (      
      <CustomCheckbox className={classes.printIcon}
        icon={card.print ? <CustomSelectedCheckbox /> : <CircleUnchecked />}
        checkedIcon={card.print ? <CircleUnchecked /> : <CustomSelectedCheckbox />}
        onChange={cardSelected}
      />) : (null)}
    </Paper>
  );
}

export default EteCard;