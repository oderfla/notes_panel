import WHITE from '../utils/constants.js';

const styles = theme => ({
    root: {
      flexGrow: 1,
      overflow: "auto",
      position: "relative",
      width: '100%',
      height: "calc(100% - 150px);",
      alignSelf: 'flex-middle',
      backgroundColor: WHITE,
      scrollBehavior: 'smooth',
      top: 3,
    },
    list: {
      height: "inherit",
    },
    paper: {
      maxWidth: 380,
      margin: `${theme.spacing(2)}px auto`,
      padding: theme.spacing(1),
      boxShadow: '0 0 5px 2px rgba(0, 0, 0, 0.16)',
      // '&:hover': {
      //   border: '2px',
      //   boxShadow: '0px 0px 5px 2px rgba(0, 0, 0, 0.16)',
      //   backgroundColor: 'rgba(0, 0, 0, 0.04)',
      // },
    },
    paperActive: {
      maxWidth: 380,
      margin: `${theme.spacing(2)}px auto`,
      padding: theme.spacing(1),
      boxShadow: '0px 0px 5px 2px rgba(0, 0, 0, 0.16)',
    },
    moreIcon: {
      position: "relative",
      float: 'right',
      bottom: 5
    },
    printIcon: {
      position: "relative",
      float: 'right',
      bottom: 5,
      zIndex: 2,
    },
    viewOnPageButton: {
      width: 135,
      heigth: 35,
      color: '#008476',
    },
    moreIconGrid: {
      height: '40px'
    },
    grid: {
      padding: theme.spacing(1),
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: 'auto',
      display: 'block',
      // maxWidth: '100%',
      // maxHeight: '100%',
      width: 98,
      height: 98,
    },
    noteContainer: {
      left: '42px',
      right: '8px',
      width: '300px',
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    textFont:{
      fontSize:14
    },
  });

  export default styles;