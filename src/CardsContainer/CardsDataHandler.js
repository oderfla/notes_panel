import React, {useMemo, useState, useRef, useEffect} from 'react';
import Card from './Card.js';

const getCards = (record, index, tab, classes, setDeletedCard, isPrintSelected, handlePrintCount) => {
return (
  <Card key={index}
    card={record}
    index={index}
    classes={classes}
    selectedTab={tab}
    setDeletedCard={setDeletedCard}
    isPrintSelected={isPrintSelected}
    handlePrintCount={handlePrintCount}
   />
)};

export const records = (data, dataSize, tab, classes, setDeletedCard, isPrintSelected, handlePrintCount) => {

  const allUsers = useMemo(
    () =>
      new Array(dataSize)
        .fill(true)
        .map((num, index) => getCards(data[index], index, tab, classes, setDeletedCard, isPrintSelected, handlePrintCount)), 
    [dataSize, isPrintSelected],
  );

  const loadedCount = useRef(0);
  const endReached = useRef(false);
  const [loadedUsers, setLoadedUsers] = useState([]);
  const TOTAL_COUNT = dataSize;

  const loadMore = () => {
    if (!endReached.current) {
      setTimeout(() => {
        loadedCount.current += 10;

        if (loadedCount.current === TOTAL_COUNT) {
          endReached.current = true;
        }

        // in a real world scenario, you would fetch the next
        // slice and append it to the existing records
        setLoadedUsers(allUsers.slice(0, loadedCount.current));
      }, 50);
    }
  };

  const resetValues = () => {
    loadedCount.current = 0;
    endReached.current = false;
    setLoadedUsers([]);
  };

  useEffect(loadMore, []);

  return {
    loadedUsers,
    loadMore,
    resetValues,
  };
};
