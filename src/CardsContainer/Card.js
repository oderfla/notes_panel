import React from "react";
import EteCard from "./EteCard.js";
import NoteCard from "./MyNoteCard.js";
import BookmarkCard from "./BookmarkCard.js";

export const Card = props => {
  const { classes, card, index, selectedTab, setDeletedCard, isPrintSelected, handlePrintCount } = props;

  function cardPicker() {

    switch (selectedTab) {
      case "CLASSROOM":
        return (<EteCard
              classes={classes}
              card={card}
              index={index}
              isPrintSelected={isPrintSelected}
              handlePrintCount={handlePrintCount}
               />)
      case "MY NOTES":
        return ( <NoteCard
          classes={classes}
          card={card}
          index={index}
          setDeletedCard={setDeletedCard}
          isPrintSelected={isPrintSelected}
          handlePrintCount={handlePrintCount}
        />)
      case "BOOKMARKS":
          return ( <BookmarkCard
            classes={classes}
            card={card}
            index={index}
            setDeletedCard={setDeletedCard}
            isPrintSelected={isPrintSelected}
            handlePrintCount={handlePrintCount}
        />)
      default:
    }
  }

  return (
    <div>
      {cardPicker()}
    </div>
  );
}

export default Card;