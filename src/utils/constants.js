//TAB names for the panel
export const CLASSROOM = 'CLASSROOM';
export const MY_NOTES = 'MY NOTES';
export const BOOKMARK = 'BOOKMARKS';

//Top menu settings options

export const PRINT = 'Select to Print';
export const PRINT_ALL = 'Print All';
export const SHARE = 'Share';

//Colors

export const WHITE = '#FFFFFF';
export const ICONS_COLOR = '#54585a';
export const GREEN = '#008476';
export const BLACK = '#000000';
export const SHADOW = "#888888";

export const TEACHER_NOTES = 99;
export const USER_NOTES = 1;
export const USER_BOOKMARKS = 2;

// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';
// export const WHITE = '#FFFFFF';