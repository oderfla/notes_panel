export default function(text = '', limit, includeElipses = false) {
  if (text.length >= limit) {
    for (let i = limit; i < text.length; i++) {
      if (text[i] === ' ') {
        text = includeElipses
          ? text.substr(0, i).concat('...')
          : text.substr(0, i);
        break;
      }
    }
  }
  return text;
}
